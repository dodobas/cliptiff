#format Makefile datoteke je sljedeci

#   target <abc>: dependencies
#                 command

#defniramo postavke kompilera
#-ltiff koristimo libtiff
FLAGS=-Wno-deprecated -ltiff
FLAGS2=-Wno-deprecated

#defniramo defaultni target <all>
all: cliptiff.o
	g++ $(FLAGS) -o cliptiff cliptiff.o

#prvi objekt
cliptiff.o: cliptiff.cpp d_qsort.h mtiff.h
	g++ $(FLAGS2) -c cliptiff.cpp

#pocisti sve
clean:
	rm cliptiff cliptiff.o