#include <iostream>
#include <tiffio.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <getopt.h>
#include "mtiff.h"
//#include "d_qsort.h"

// potrebno za pretrazivanje direktorija
#include <stdlib.h>
#include <dirent.h>


using namespace std;

const string versionstring="TIFFImageClipper v0.1.14\n";
const string helpstring="\nDescription:\nUse this program to clip a large TIFF image into smaller images(clips). Each new\nclip file is named 'filename_xxx.tif' where <xxx> is a number of newly created\nimage.\n\nUsage:\n\tcliptiff [OPTIONS]\n\nGeneral options:\n\n  --help or -h\t\t  Show this help, then exit\n\n  --version or -v\t  show program version\n\n  --file or -f FILENAME   uses FILENAME as primary input file. User MUST specify\n\t\t\t  full FILENAME, i.e. 'a.tif', and not only 'a'\n\n  --clips or -c AxB\t  makes total of A*B clips, A defines number of clips\n\t\t\t  per image width, and B defines number of clips per\n\t\t\t  image height. If NOT specified DEFAULT 2x2 value is\n\t\t\t  used!\n\n  --smart or -s\t\t  Program tries to be smart and clips an image if actual\n\t\t\t  size of an array that image covers is predefinied \n\n  --outputdirectory or -o all clips are saved in this directory, if not \n\t\t\t  specified current working directory is used\n\n  --directory or -d\t  all .tif files inside specified directory are going \n\t\t\t  to be processed\n\n  -smartdirectory or -m\t  cliped images are going to be saved inside predefined \n\t\t\t  directory structure. This option also switches the \n\t\t\t  --smart option\n\nExample:\n\tcliptiff -f a.tif -c 4x5\n\nThis command would create total of 4*5=20 smaller image clips of a large tif\nfile 'a.tif', with clips names starting from 'a_001.tif' to 'a_020.tif'.\n\nPlease report any problem or bugs to <dodobas@open.geof.hr>.";

struct ClipTIFFSettings{
  string input;
  bool help;
  bool version;
  bool besmart;
  int numperw;
  int numperh;
  string process_directory;
  string output_directory;
  string smart_directory;
  };

struct fileNames{
  string *filename;
  int number_of_files;
};

//int sortIndex();

fileNames tif_filename(string dirname);
string check_directory_name(string directory);
string get_cwd();
int clipit(ClipTIFFSettings ct_settings,bool smartdirectory);


/*  ClipTIFFSettings argvParser(int num, char *argv[]);*/

int main(int argc, char* argv[]) { 
  ClipTIFFSettings ct_settings;

  ct_settings.help=false;
  ct_settings.version=false;
  ct_settings.input="";
  ct_settings.numperw=2;
  ct_settings.numperh=2;
  ct_settings.besmart=false;
  ct_settings.process_directory="";
  ct_settings.output_directory=check_directory_name(get_cwd());
  ct_settings.smart_directory="";

  //postavi osnovne varijable

  int c=0;

 struct option long_options[]={
    {"version",0,NULL,'v'},
    {"file",1,NULL,'f'},
    {"clips",1,NULL,'c'},
    {"help",0,NULL,'h'},
    {"smart",0,NULL,'s'},
    {"directory",1,NULL,'d'},
    {"outputdirectory",1,NULL,'o'},
    {"smartdirectory",1,NULL,'m'},
    {0,0,0,0}
  };

 if(argc==1){
   cout<<versionstring;
   cout<<"Try 'cliptiff --help' for more information."<<endl;
 }
   
  while((c=getopt_long(argc,argv,"d:shvf:c:o:m:",long_options, NULL))!=-1){
    
    switch(c){
    case 'h':
      ct_settings.help=true;
      break;
    case 'v':
      ct_settings.version=true;
      break;
    case 'f':
      //      printf("opcija input\n");
      ct_settings.input=optarg;
      //      cout<<optarg<<endl;
      break;
    case 'c':
      {
	//printf("opcija clips\n");
	//cout<<optarg<<endl;
      string astr=optarg;
      int pos = astr.find_first_of("Xx", 0);
      if((pos==0)||(pos==-1)){
	cout<<"Incorrect use of --clip option. Correct use would be something like \n\t\"--clip 2x4\" or\n \t\"-c 8X5\"\n";
	exit(1);
      }
      string snw = astr.substr(0,pos);
      string snh = astr.substr(pos+1,astr.length()-pos);

      ct_settings.numperw=atoi(snw.c_str());
      ct_settings.numperh=atoi(snh.c_str());

      //DBG napravi provjeru ako su numperw = numperh = 0

      //      cout <<pos<<" "<<snw<<" "<<snh<<endl;
      //      cout <<pos<<" "<<ct_settings.numperw<<" "<<ct_settings.numperh<<endl;
    }
      break;
    case 's':
      ct_settings.besmart=true;
      break;
    case 'd':
      ct_settings.process_directory=check_directory_name(optarg);
      break;
    case 'o':
      ct_settings.output_directory=check_directory_name(optarg);
      break;
    case 'm':
      ct_settings.smart_directory=check_directory_name(optarg);
      ct_settings.besmart=true;
      //cout<<"Radi"<<ct_settings.smart_directory<<endl;
      //exit(1);
      break;

    case '?':
    default:
      cout<<"Try 'cliptiff --help' for more information."<<endl;
      break;
    }
  }

  if (optind < argc) {
    printf ("non-option ARGV-elements: ");
    while (optind < argc)
      printf ("%s ", argv[optind++]);
    printf ("\n");
  }

  if (ct_settings.help){
    cout<<versionstring<<helpstring<<endl;
    return 1;
  }
  if (ct_settings.version){
    cout<<versionstring<<endl;
    return 0;
  }

  //Procesiranje direktorija
  if ((ct_settings.process_directory!="")&&(ct_settings.smart_directory=="")){

    fileNames fn=tif_filename(ct_settings.process_directory);

    for(int i=0;i<fn.number_of_files;i++){
      //processing loop
      cout <<fn.filename[i]<<endl;
      ct_settings.input=fn.filename[i];
      clipit(ct_settings,false);
      
    }
    //    cout <<ct_settings.process_directory<<endl;
    //    cout <<ct_settings.output_directory<<endl;
    return 0;
    }

  if((ct_settings.smart_directory!="")&&(ct_settings.input!="")&&(ct_settings.besmart)){
    //    cout<<"broj3"<<endl;exit(1);
    ct_settings.output_directory=ct_settings.smart_directory;
    clipit(ct_settings,true);
    return 0;
  }

  if((ct_settings.smart_directory!="")&&(ct_settings.process_directory!="")&&(ct_settings.besmart)){
    //    cout<<"broj4"<<endl;exit(1);
    ct_settings.output_directory=ct_settings.smart_directory;
    
    fileNames fn=tif_filename(ct_settings.process_directory);

    for(int i=0;i<fn.number_of_files;i++){
      //processing loop
      cout <<fn.filename[i]<<endl;
      ct_settings.input=fn.filename[i];
      clipit(ct_settings,true);
      
    }

    return 0;
  }  
  
  if (ct_settings.input!=""){
    clipit(ct_settings,false);
    return 0;
  }
  cout << "nepoznata kombinacija parametara"<<endl;
  return(0);
} 




fileNames tif_filename(string dirname){
  struct dirent *filelist;
  DIR *dp;
  dp = opendir(dirname.c_str());
  
  string tmp;
  string namelist[255];

  int n=0;

  while ((filelist = readdir(dp)) != NULL) {
    tmp = filelist->d_name;

    int gdje_je_tocka=tmp.find_last_of(".",tmp.length());
  
      if (gdje_je_tocka>0){

	tmp=tmp.substr(gdje_je_tocka,tmp.length());
	if(tmp.compare(".tif")==0){

	  //	  cout << filelist->d_name<<endl;  
	  namelist[n]=filelist->d_name;
  	  n++;
	}
    }
  }
  if(n>0){
    fileNames fn;
    fn.number_of_files=n;
    fn.filename=new string[n];
    
    while(n--) {
      
      fn.filename[n]=dirname+namelist[n];
      //cout << fn.filename[n]<<endl;
    } 

    //    exit(1);
    return fn;
  }
    //break

}

string check_directory_name(string directory){
  //check the ending of directory name, smisliti nacin da radi neovisno
  //o operativnom sustavu Windowsi '\' Linux '/'

  char ending=directory.at(directory.length()-1);
  if (ending!='/'){     
    directory+="/";
  }

//  cout <<ending<<endl;
//  cout <<directory<<endl;
  return directory;
}

string get_cwd(){
  char *wd=new char[256];
  getcwd(wd,128);

  //  cout <<wd<<endl;

  return wd;
}

int clipit(ClipTIFFSettings ct_settings,bool smartdirectory){
 
  cClipTIFF *ct=new cClipTIFF(ct_settings.input,ct_settings.output_directory,smartdirectory);
  //make the actual clips
  if(ct_settings.besmart){
    ct->makeSmartClips();
  }
  else if (ct_settings.besmart!=true){
    ct->makeClips(0,ct_settings.numperw,ct_settings.numperh);
  }    
  //    int clips=ct_settings.numperw*ct_settings.numperh;
  
  int clips=ct->num_of_clps;
  //save created clips
  
  for(int i=0;i<clips;i++){
    //cout<<"ime: "<<ct->clipdata[i].ime<<endl;
    //cout<<"x1: "<<ct->clipdata[i].x1<<endl;
    //cout<<"y1: "<<ct->clipdata[i].y1<<endl;
    //cout<<"x2: "<<ct->clipdata[i].x2<<endl;
    //cout<<"y2: "<<ct->clipdata[i].y2<<endl<<endl;
    
    ct->clipTiff(ct->clipdata[i].x1,ct->clipdata[i].y1,ct->clipdata[i].x2,ct->clipdata[i].y2,ct->clipdata[i].name);
  }
  //save the index
  ct->createIndex();
  //sort the index
  ct->sortIndex();
  delete ct;// rjesenje za "BIG memory problem"
}

