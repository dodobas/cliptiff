typedef struct t_indexdata{
  string ime;
  double miny,minx,maxy,maxx;
};

const int MaxArray = 5000;

typedef t_indexdata indexdata[MaxArray];

void QuickSort(indexdata Num, int Lower, int Upper);

int Partition(indexdata Num, int Lower, int Upper);


inline void Swap(t_indexdata & First, t_indexdata & Second){
  t_indexdata Temp[1];
  Temp[0] = First;
  First = Second;
  Second = Temp[0];
  //   cout <<First.ime<<" "<<Second.ime<<endl;
}

void QuickSort(indexdata NumArray, int Lower, int Upper){
  int PivotIndex;
  
  if (Lower < Upper){
    PivotIndex = Partition(NumArray, Lower, Upper);
    QuickSort(NumArray, Lower, PivotIndex - 1);   // sort left side
    QuickSort(NumArray, PivotIndex + 1, Upper);   // sort right side
  }
}


int Partition(indexdata NumArray, int Lower, int Upper){
  double Pivot;
  int  Left, Right;
  t_indexdata tempos[1];
  tempos[0]=NumArray[Lower];
  Pivot = NumArray[Lower].miny;
  Left = Lower;
  Right = Upper;
  
  while (Left < Right){
    // scan from left, skipping items that belong there
    while ((NumArray[Left].miny <= Pivot) && (Left < Upper))
      Left++;
    // scan from right, skipping items that belong there
    while (NumArray[Right].miny > Pivot)
      Right--;
      if (Left < Right)
	//	cout <<"swaping";
	Swap(NumArray[Left], NumArray[Right]);
  }
  
  NumArray[Lower] = NumArray[Right];
  NumArray[Right] = tempos[0];
  return Right;  // return the pivot index
}


