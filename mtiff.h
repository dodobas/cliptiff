#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <tiffio.h>
//#include <string>
#include <string.h>
#include <sstream>
#include <stdio.h>
//#include "d_qsort.h"

using namespace std;

typedef struct t_indexdata{
  string ime;
  double miny,minx,maxy,maxx;
};

const int MaxArray = 5000;

typedef t_indexdata indexdata[MaxArray];

void QuickSort(indexdata Num, int Lower, int Upper);

int Partition(indexdata Num, int Lower, int Upper);




class cClipTIFF {
public:
  ~cClipTIFF();
  //  cClipTIFF();
  cClipTIFF(string name,string output_directory,bool smartdirectory);
  int clipTiff(uint16 x1,uint16 y1,uint16 x2,uint16 y2,string naziv);
  uint16 getImageHeight();
  uint16 getImageWidth();
  int makeClips(int smart_index,int nw,int nh);
  int makeSmartClips();
  int createIndex();
  int sortIndex();
  double XgeoC(uint16 xpix,uint16 ypix);
  double YgeoC(uint16 xpix,uint16 ypix);
  struct t_clipdata{
    string name;
    uint16 x1,y1,x2,y2;

/*     0 => bez mjerila */
/*     1 => 1000 */
/*     2 => 2880 */
/*     3 => 5000 */
/*     4 => 10000 */
/*     5 => 25000 */
/*     6 => 50000 */
/*     7 => 100000 */
  };
  t_clipdata *clipdata;
  int num_of_clps;

private:
  int saveTiff(string name,uint32* raster,uint16 imageheight,uint16 imagewidth);
  int readTFW(string name);
  int saveTFW(string naziv, double xgeo,double ygeo);
  
  bool smartdirectory;
  string smart_directory;
  uint16 imageheight;
  uint16 imagewidth;
  uint16 resunit;
  float xres;
  float yres;
  TIFF *tiff;
  uint32 *raster;
  string dname;
  double A,B,C,D,E,F;
  string output_directory;
  string pt_name;//prototype name
};

int cClipTIFF::saveTiff(string name,uint32* raster,uint16 imageheight,uint16 imagewidth){
  TIFF* tif = TIFFOpen(name.c_str(), "w");
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
  TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, 2);
  TIFFSetField(tif, TIFFTAG_PLANARCONFIG, 1);
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 4);

  TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageheight);
  TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imagewidth);
  // problemi u autocadu
  //  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE);
  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_PACKBITS);
  //  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
  TIFFSetField(tif, TIFFTAG_XRESOLUTION,this->xres);
  TIFFSetField(tif, TIFFTAG_YRESOLUTION,this->yres);
  TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT,this->resunit);

  long brojpixela=imagewidth*imageheight*4;
  //konacno zapisi datoteku
  if(TIFFWriteEncodedStrip(tif,0,raster,brojpixela)){
    TIFFClose(tif);
    return(1);
  }
  return(0);
}

cClipTIFF::cClipTIFF(string name,string output_directory,bool smartdirectory){

  this->output_directory=output_directory;
  this->smartdirectory=smartdirectory;
  string clear_name;
  clear_name=name.substr(0,name.length()-4);
  this->dname=clear_name;
  //create prototypename
  this->pt_name=clear_name.substr(clear_name.find_last_of("/",clear_name.length())+1,clear_name.length());
  cout <<this->pt_name<<endl;
  

  //  exit(1);
  

  this->tiff = TIFFOpen(name.c_str(), "r"); 
  TIFF *tif=this->tiff;

  uint32 imageheight;
  if(TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageheight)==0){
    cout << "E_RR_OR prilikom pristupa TIFFTAG_IMAGELENGTH..." <<endl;
    exit(1);
  }
  else {
    //cout << "TIFFTAG_IMAGELENGTH " << imageheigth<<endl;
    this->imageheight=imageheight;
  }
  uint32 imagewidth;
  if(TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth)==0){
    cout << "E_RR_OR prilikom pristupa TIFFTAG_IMAGEWIDTH..." <<endl;
    exit(1);
  }
  else {
    //cout << "TIFFTAG_IMAGEWIDTH " << imagewidth<<endl;
    this->imagewidth=imagewidth;
  }
  float xres;
  if(TIFFGetField(tif, TIFFTAG_XRESOLUTION, &xres)==0){
    cout << "E_RR_OR prilikom pristupa TIFFTAG_XRESOLUTION..." <<endl;
    exit(1);
  }
  else {
    //cout << "TIFFTAG_XRESOLUTION " << xres<<endl;
    this->xres=xres;
  }

  float yres;
  if(TIFFGetField(tif, TIFFTAG_YRESOLUTION, &yres)==0){
    cout << "E_RR_OR prilikom pristupa TIFFTAG_YRESOLUTION..." <<endl;
    exit(1);
  }
  else {
    //cout << "TIFFTAG_YRESOLUTION " << yres<<endl;
    this->yres=yres;
  }

  uint16 resunit;
  if(TIFFGetField(tif, TIFFTAG_RESOLUTIONUNIT, &resunit)==0){
    cout << "E_RR_OR prilikom pristupa TIFFTAG_RESOLUTIONUNIT..." <<endl;
    exit(1);
  }
  else {
    //cout << "TIFFTAG_RESOLUTIONUNIT " << resunit<<endl;
    this->resunit=resunit;
  }

  //ucitaj sliku u memoriju i konvertiraj u dvodimenzionalno polje
  this->raster = (uint32*) _TIFFmalloc(imagewidth * imageheight * sizeof (uint32)); 
  
  if (raster) { 
    // ucitavamo sliku u memoriju te je ispravno orijentiramo tako da
    // se koordinte 0,0 nalaze u gornjem ljevom kutu

    //cout<<"Ucitavam... "<<this->dime<<" velicina "<<this->imagewidth<<"x"<<this->imageheight<<endl;

    if (TIFFReadRGBAImageOriented(this->tiff, imagewidth, imageheight, this->raster,ORIENTATION_TOPLEFT ,0)) {
      cout <<"TIFF je ispravno prepoznat i ucitan u memoriju!"<<endl;
    }
    else{
      cout<<"TIFF nije ispravno prepoznat te nije ucitan u memoriju!"<<endl;
      exit(1);
    }

  }
  string tfw_name;
  //tfw_name=name.substr(0,name.length()-4);
  tfw_name=clear_name+".tfw";
  //  cout << tfw_name<<"\n";
  this->readTFW(tfw_name);
}

//destruktor 
cClipTIFF::~cClipTIFF(){
  delete [] this->clipdata;
  _TIFFfree(this->raster);
}

int cClipTIFF::clipTiff(uint16 x1,uint16 y1,uint16 x2,uint16 y2,string naziv) {
  uint16 wid=0;
  uint16 hei=0;
  uint16 swp=0;
  if(x1<x2){
    wid=x2-x1;
  }
  else{
    wid=x1-x2;
    swp=x1;
    x1=x2;
    x2=swp;
  }
  if(y1<y2){
    hei=y2-y1;
  }
  else{
    hei=y1-y2;
    swp=y1;
    y1=y2;
    y2=swp;
  }
  if(wid>this->imagewidth){
    cout<<"Nesto je trulo u drzavi Danskoj...\n"<<wid<<" > "<<this->imagewidth<<" width"<<endl;
    exit(1);
  }
  if(hei>this->imageheight){
    cout<<"Nesto je trulo u drzavi Danskoj...\n"<<hei<<" > "<<this->imageheight<<" heigth"<<endl;
    exit(1);
  }

  int brojpix=0;
  
  uint32 *nraster;
  nraster = (uint32*) _TIFFmalloc((wid *hei) * sizeof (uint32)); 

  /*cout << x1<<" "<<y1<<endl;*/
    double xgeo=XgeoC(x1,y1);
    double ygeo=YgeoC(x1,y1);

      //cout <<xgeo<<endl;
      //cout <<ygeo<<endl;
  
    //bug s nestajanjem pixela nije bug nego problem s image smoothing
    //u acadu je jako iritantan i blesav

  for(int i=y1;i<y2;i++){
    for(int j=x1;j<x2;j++){
      //nraster[((i-y1)*(x2-x1)+(j-x1))]=this->platno[i][j];
      nraster[((i-y1)*(x2-x1)+(j-x1))]=this->raster[i*this->imagewidth+j];
      brojpix++;
    }
  }
  // cout <<"Broj pix: "<<brojpix<<endl;
  // cout<< "Naziv "<<naziv<<"\n";
  string tfw_name;
  tfw_name=naziv.substr(0,naziv.length()-4);
  //  cout<< "TFW "<<tfw_name<<"\n";
  tfw_name=tfw_name+".tfw";

  //cout << tfw_name<<"\n";
 
  cout <<"Saving: "<<naziv<<" size "<<wid<<"x"<<hei<<endl;
  this->saveTiff(naziv,nraster,hei,wid);
  cout<<"Saving TFW: "<<tfw_name<<endl;
  this->saveTFW(tfw_name,xgeo,ygeo);
  //cout <<"Snimljeno..."<<endl;
  _TIFFfree(nraster);
}

uint16 cClipTIFF::getImageWidth() {
  return this->imagewidth;
}

uint16 cClipTIFF::getImageHeight() {
  return this->imageheight;
}

int cClipTIFF::makeClips(int smart_index,int nw, int nh){

  int numberofclips=0;
  if ((nw)&&(nh)) {
    numberofclips=nw*nh;
  }
  this->num_of_clps=numberofclips;
    cout<<"Making "<<numberofclips<<" clips!"<<endl;
  uint16 clipwidth=(uint16)(this->imagewidth/nw);
  uint16 clipheight=(uint16)(this->imageheight/nh);
  //cout <<clipwidth<<"X"<<clipheight<<endl;
  this->clipdata =new t_clipdata[numberofclips];

  uint16 tx1=0;
  uint16 ty1=0;
  uint16 tx2=0;
  uint16 ty2=0;
  int brojac=0;
  int tbrojac=0;

  for(int i=0;i<nw;i++){
    ty1=0;
    tx2=tx1+clipwidth;
    if((tx2+clipwidth)>this->imagewidth){
      tx2=this->imagewidth;
    }
    for(int j=0;j<nh;j++){
      ty2=ty1+clipheight;
      if((ty2+clipheight)>this->imageheight){
	ty2=this->imageheight;
      }

      tbrojac=(brojac+1);
      string tmpa;
      ostringstream ss;
      if(tbrojac<10){
	ss << this->pt_name << "_00" << tbrojac<<".tif";
      }
      else if (tbrojac<100){
	ss << this->pt_name << "_0" << tbrojac<<".tif";
      }
      else{
	ss <<this->pt_name<<"_"<<tbrojac<<".tif";
      }

      //      string smart_dir="";
      this->smart_directory="";
      if((smart_index!=0)&&(this->smartdirectory)){
	switch(smart_index){
	case 1:
	  this->smart_directory="1000/";
	  break;
	case 2:
	  this->smart_directory="2880/";
	  break;
	case 3:
	  this->smart_directory="5000/";
	  break;
	case 4:
	  this->smart_directory="10000/";
	  break;
	case 5:
	  this->smart_directory="25000/";
	  break;
	case 6:
	  this->smart_directory="50000/";
	  break;
	case 7:
	  this->smart_directory="100000/";
	  break;
	}
      }

      tmpa = this->output_directory+this->smart_directory+ss.str();
      
      this->clipdata[brojac].name=tmpa;

      this->clipdata[brojac].x1=tx1;
      
      this->clipdata[brojac].y1=ty1;
      
      this->clipdata[brojac].x2=tx2;
      
      this->clipdata[brojac].y2=ty2;
      
      //dodano zbog 'pametnih direktorija'


      
      //cout<<"ime: "<<this->clipdata[brojac].ime<<endl;
      //cout<<"x1: "<<this->clipdata[brojac].x1<<endl;
      //cout<<"y1: "<<this->clipdata[brojac].y1<<endl;
      //cout<<"x2: "<<this->clipdata[brojac].x2<<endl;
      //cout<<"y2: "<<this->clipdata[brojac].y2<<endl<<endl;

      brojac++;

      ty1=ty2;
    }
    tx1=tx2;
  }

  // cout <<clipwidth<<"X"<<clipheight<<" OK!"<<endl;
  return 1;
}

int cClipTIFF::readTFW(string name){
  /*  FILE *tfwfile;
      tfwfile = fopen(name.c_str(),"r");*/
  string red;
  ifstream tfwfile(name.c_str());
  if(! tfwfile.is_open()){
    cout<<"error opening file: "<<name;
    exit(1);
  }

  getline(tfwfile,red);
  //  std::istringstream(red) >> this->A;
  this->A=atof(red.c_str());
  //cout<<red<<endl;
  //cout<<A<<endl;
  //tfwfile.getline(red,100);
  getline(tfwfile,red);
  //  this->B=(double)strtod(red,0);
  std::istringstream(red) >> this->B;
  //cout<<red<<endl;
  //cout<<B<<endl;
  //tfwfile.getline(red,100);
  getline(tfwfile,red);  
  //this->C=(double)strtod(red,0);
  std::istringstream(red) >> this->C;
  //cout<<red<<endl;
  //cout<<C<<endl;
  //  tfwfile.getline(red,100);
  getline(tfwfile,red);
  //this->D=(double)strtod(red,0);
  std::istringstream(red) >> this->D;
  //cout<<red<<endl;
  //cout<<D<<endl;
  //  tfwfile.getline(red,100);
  getline(tfwfile,red);http://www.google.com/
  //  this->E=(double)strtod(red,0);
  //std::istringstream(red) >> this->E;
  this->E=atof(red.c_str());
  //cout<<red<<endl;    
  //cout<<(E)<<endl;
  //tfwfile.getline(red,100);
  getline(tfwfile,red);
  //  this->F=(double)strtod(red,0);
  //  std::istringstream(red) >> this->F;
  this->F=atof(red.c_str());
  //cout<<red<<endl;
  //cout.precision(20);
  //cout<<(double)F<<endl;
  
}

double cClipTIFF::XgeoC(uint16 xpix,uint16 ypix){
  return (this->E+xpix*this->A+ypix*this->C);
}

double cClipTIFF::YgeoC(uint16 xpix,uint16 ypix){
  return (this->F+ypix*this->D+ypix*this->B);
}
 
int cClipTIFF::saveTFW(string naziv, double xgeo,double ygeo){
  ofstream tfwfile (naziv.c_str());
  if (tfwfile.is_open())
  {
    tfwfile.setf(ios::fixed, ios::floatfield);
    tfwfile.setf(ios::showpoint);
    tfwfile.precision(15);
    tfwfile << this->A<<endl;
    tfwfile << this->B<<endl;
    tfwfile << this->C<<endl;
    tfwfile << this->D<<endl;
    tfwfile << xgeo<<endl;
    tfwfile << ygeo<<endl;
    tfwfile.close();
  }
}

int cClipTIFF::makeSmartClips(){
  cout << this->imagewidth<< "x"<<this->imageheight<<endl;
  double X_m=0;
  double Y_m=0;
  X_m=this->XgeoC(this->imagewidth,0)-this->XgeoC(0,0);
  Y_m=this->YgeoC(0,0)-this->YgeoC(0,this->imageheight);
  //cout << X_m<<endl;
  //  cout << Y_m<<endl;

  if(((0.95*X_m)<860)&&(860<(1.05*X_m))){
    cout<<"Mjerilo je 1:1000"<<endl;
    this->makeClips(1,4,3);
  }
  else if(((0.95*X_m)<1925)&&(1925<(1.05*X_m))){
    cout<<"Mjerilo je 1:2880"<<endl;
    this->makeClips(2,4,3);
  }
  else if(((0.95*X_m)<2250)&&(2250<(1.05*X_m))){
    cout<<"Mjerilo je 1:5000"<<endl;
    this->makeClips(3,3,6);
  }
  else if(((0.95*X_m)<9770)&&(9770<(1.05*X_m))){
    cout<<"Mjerilo je 1:25000"<<endl;
    this->makeClips(5,10,14);
  }
  else{
    cout << "Duzina sirine slike: "<<X_m<<endl;
    cout << "Duzina visine slike: "<<Y_m<<endl<<endl;
    cout << "Posaljite mailom ove podatke zajedno s mjerilom karte kako bih mogao napraviti 'UPDATE' programa!\nPostoji problem kada se tif mora rotirati!"<<endl<<endl;
    cout << "I can't be smart, but there is someone who can make me smarter so please contact him. :)\nHINT: dodobas@open.geof.hr\n"<<endl;
    exit(1);
  }
  return 0;
}

int cClipTIFF::createIndex(){
  string index_name=this->output_directory+this->smart_directory+"index.ix";

  cout << "Creating index..."<<index_name<<"...";
  ofstream indexfile (index_name.c_str(),ios::app);
  if (indexfile.is_open())
  {
    for(int i=0;i<num_of_clps;i++){

      indexfile <<this->clipdata[i].name<<";";
      indexfile.setf(ios::fixed, ios::floatfield);
      indexfile.setf(ios::showpoint);
      indexfile.precision(2);
      
      //geodetske koordinate min,min - max.max

      indexfile <<this->XgeoC(this->clipdata[i].x1,this->clipdata[i].y2)<<";";
      indexfile <<this->YgeoC(this->clipdata[i].x1,this->clipdata[i].y2)<<";";
      indexfile <<this->XgeoC(this->clipdata[i].x2,this->clipdata[i].y1)<<";";
      indexfile <<this->YgeoC(this->clipdata[i].x2,this->clipdata[i].y1)<<"\n";
      
      //slikovne koordinate max,min - min,max 
      /*
      indexfile <<this->XgeoC(this->clipdata[i].x1,this->clipdata[i].y1)<<";";
      indexfile <<this->YgeoC(this->clipdata[i].x1,this->clipdata[i].y1)<<";";
      indexfile <<this->XgeoC(this->clipdata[i].x2,this->clipdata[i].y2)<<";";
      indexfile <<this->YgeoC(this->clipdata[i].x2,this->clipdata[i].y2)<<"\n";
      */

    }
    indexfile.close();
    cout<<"DONE!"<<endl;
    return 0;
  }
  cout<<"FAILED!"<<endl;
  exit(1);
  return 0;
}

int cClipTIFF::sortIndex(){
  int broj=0;
  string red;
  /*  struct t_indexdata{
    string ime;
    double miny,minx,maxy,maxx;
  };
  t_indexdata *indexdata;*/

  indexdata indexdataloc;
  string index_name=this->output_directory+this->smart_directory+"index.ix";
  ifstream index_file(index_name.c_str());

  if(! index_file.is_open()){
    cout<<"error opening file: "<<index_name;
    exit(1);
  }
  //  indexdata =new t_indexdata[5000];
  int pozicija=0;
  string dio;
  while(!(index_file.eof())){
    getline(index_file,red);
    pozicija=red.find_first_of(";",0);
    if(pozicija!=-1){
      dio = red.substr(0,pozicija);
      red.erase(0,pozicija+1);
      indexdataloc[broj].ime=dio;

      pozicija=red.find_first_of(";",0);
      dio = red.substr(0,pozicija);
      red.erase(0,pozicija+1);
      indexdataloc[broj].miny=atof(dio.c_str());
    
      pozicija=red.find_first_of(";",0);
      dio = red.substr(0,pozicija);
      red.erase(0,pozicija+1);
      indexdataloc[broj].minx=atof(dio.c_str());

      pozicija=red.find_first_of(";",0);
      dio = red.substr(0,pozicija);
      red.erase(0,pozicija+1);
      indexdataloc[broj].maxy=atof(dio.c_str());

      //pozicija=red.find_first_of(";",0);
      dio = red.substr(0,red.length());
      indexdataloc[broj].maxx=atof(dio.c_str());
      broj++;
    }
  }
  /*  cout <<"Tucam"<<endl;
  for(int i=0;i<broj;i++){
    cout<<indexdata[i].ime<<" "<<indexdata[i].miny<<" "<<indexdata[i].minx<<" "<<indexdata[i].maxy<<" "<<indexdata[i].maxx<<endl;
    }*/
  /*t_indexdata tmp[1];
  tmp[0]=indexdataloc[1];
  indexdataloc[1]=indexdataloc[2];
  indexdataloc[2]=tmp[0];
  cout <<indexdataloc[1].ime<<" "<<indexdataloc[2].ime<<endl;*/
  QuickSort(indexdataloc,0,broj-1);

  cout.setf(ios::fixed, ios::floatfield);
  cout.setf(ios::showpoint);
  cout.precision(2);

  /*  for(int i=0;i<broj;i++){
    cout<<indexdataloc[i].ime<<" "<<indexdataloc[i].miny<<" "<<indexdataloc[i].minx<<" "<<indexdataloc[i].maxy<<" "<<indexdataloc[i].maxx<<endl;
    }*/

  cout << "Sorting index..."<<index_name<<"...";
  
  ofstream indexfile (index_name.c_str(),ios::trunc);
  if (indexfile.is_open())
    {
      for(int i=0;i<broj;i++){
	
	indexfile <<indexdataloc[i].ime<<";";
	indexfile.setf(ios::fixed, ios::floatfield);
	indexfile.setf(ios::showpoint);
	indexfile.precision(2);
	
	//geodetske koordinate min,min - max.max

	indexfile <<indexdataloc[i].miny<<";";
	indexfile <<indexdataloc[i].minx<<";";
	indexfile <<indexdataloc[i].maxy<<";";
	indexfile <<indexdataloc[i].maxx<<"\n";
      }
    }    
  indexfile.close();
  //  delete [] indexdataloc;
  cout << "..done!"<<endl;
  return 1;
}


inline void Swap(t_indexdata & First, t_indexdata & Second){
  t_indexdata Temp[1];
  Temp[0] = First;
  First = Second;
  Second = Temp[0];
  //   cout <<First.ime<<" "<<Second.ime<<endl;
}

void QuickSort(indexdata NumArray, int Lower, int Upper){
  int PivotIndex;
  
  if (Lower < Upper){
    PivotIndex = Partition(NumArray, Lower, Upper);
    QuickSort(NumArray, Lower, PivotIndex - 1);   // sort left side
    QuickSort(NumArray, PivotIndex + 1, Upper);   // sort right side
  }
}


int Partition(indexdata NumArray, int Lower, int Upper){
  double Pivot;
  int  Left, Right;
  t_indexdata tempos[1];
  tempos[0]=NumArray[Lower];
  Pivot = NumArray[Lower].miny;
  Left = Lower;
  Right = Upper;
  
  while (Left < Right){
    // scan from left, skipping items that belong there
    while ((NumArray[Left].miny <= Pivot) && (Left < Upper))
      Left++;
    // scan from right, skipping items that belong there
    while (NumArray[Right].miny > Pivot)
      Right--;
      if (Left < Right)
	//	cout <<"swaping";
	Swap(NumArray[Left], NumArray[Right]);
  }
  
  NumArray[Lower] = NumArray[Right];
  NumArray[Right] = tempos[0];
  return Right;  // return the pivot index
}
